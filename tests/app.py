from flask import Flask, jsonify

from flask_json_api import FlaskJsonApi

app = Flask(__name__)


class ExpectedError(Exception):
    """Expected error, such as Data Validation Error"""


class UnexpectedError(Exception):
    """Unexpected error, such as Database is not accessible"""


flask_json_api = FlaskJsonApi(app)
flask_json_api.register_expected_exception(ExpectedError)
flask_json_api.register_unexpected_exception(UnexpectedError)
flask_json_api.set_application_json_exclude_paths(['/home'])


@app.route('/home')
def home_page():
    return 'Home'


@app.route('/api')
def api():
    return jsonify(dict(message='ok'))


@app.route('/api-with-expected-error')
def api_with_expected_error():
    raise ExpectedError('Expected error')


@app.route('/api-with-unexpected-error')
def api_with_unexpected_error():
    raise UnexpectedError('Unexpected error')


@app.route('/api-with-global-error')
def api_with_global_error():
    raise Exception('Global error')
